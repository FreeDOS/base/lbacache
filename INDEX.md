# LBAcache

Disk cache for drives and floppies. Caches reads for max 8 CHS / LBA hard disks and floppies, XMS, 386 or better - tickle comes with lbacache!

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## LBACACHE.LSM

<table>
<tr><td>title</td><td>LBAcache</td></tr>
<tr><td>version</td><td>2009feb06</td></tr>
<tr><td>entered&nbsp;date</td><td>2022-01-12</td></tr>
<tr><td>description</td><td>Disk cache for drives and floppies</td></tr>
<tr><td>summary</td><td>Disk cache for drives and floppies. Caches reads for max 8 CHS / LBA hard disks and floppies, XMS, 386 or better - tickle comes with lbacache!</td></tr>
<tr><td>keywords</td><td>cache, smartdrv, nwcache</td></tr>
<tr><td>author</td><td>Eric Auer</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer</td></tr>
<tr><td>original&nbsp;site</td><td>http://auersoft.eu/</td></tr>
<tr><td>mirror&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/lbacache</td></tr>
<tr><td>platforms</td><td>DOS 386+ (nasm assembler), FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
